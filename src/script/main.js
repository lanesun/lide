/*
~
~ 程序的启动入口
~ 对对对，这才是真正的main函数。。。
~
*/

;moudle = async (self, core, require) => {
    const
    dom = await require('lib/dommer'),
    app = await require('var/app'),
    view_manager = await require('script/view-manager');

    app.menu_bar_elem = dom.id("menu_bar");
    app.tools_bar_elem = dom.id("tools_bar");
    app.left_side_bar_elem = dom.id("left_side_bar");
    app.right_side_bar_elem = dom.id("right_side_bar");
    app.main_view_elem = dom.id("main_view");
    app.output_view_elem = dom.id("output_view");
    app.status_bar_elem = dom.id("status_bar");

    view_manager.refreshView();
    view_manager.bindHandle();
};
