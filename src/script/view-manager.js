/*
~
~ UI布局器
~
*/

;moudle = async function (self, core, require) {
    const
        CONST = await require('var/const'),
        dom = await require('lib/dommer');
    self.refreshView = () => {
        dom.setVar("--menu-bar-height", CONST.MENU_BAR_HEIGHT + "px");
        dom.setVar("--tools-bar-height", CONST.TOOLS_BAR_HEIGHT + "px");
        dom.setVar("--left-side-bar-width", CONST.LEFT_SIDE_BAR_WIDTH + "px");
        dom.setVar("--right-side-bar-width", CONST.RIGHT_SIDE_BAR_WIDTH + "px");
        dom.setVar("--output-view-height", CONST.OUTPUT_VIEW_HEIGHT + "px");
        dom.setVar("--status-bar-height", CONST.STATUS_BAR_HEIGHT + "px");
    };
    self.bindHandle = () => {
        let left_resize_bar_pushed = false;
        let right_resize_bar_pushed = false;
        let bottom_resize_bar_pushed = false;
        dom.id("left_resize_bar").addEventListener('pointerdown', (e) => {
            left_resize_bar_pushed = true;
        });
        dom.id("right_resize_bar").addEventListener('pointerdown', (e) => {
            right_resize_bar_pushed = true;
        });
        dom.id("bottom_resize_bar").addEventListener('pointerdown', (e) => {
            bottom_resize_bar_pushed = true;
        });
        dom.id("left_resize_bar").addEventListener('pointerup', (e) => {
            left_resize_bar_pushed = false;
        });
        dom.id("right_resize_bar").addEventListener('pointerup', (e) => {
            right_resize_bar_pushed = false;
        });
        dom.id("bottom_resize_bar").addEventListener('pointerup', (e) => {
            bottom_resize_bar_pushed = false;
        });
        dom.id("left_resize_bar").addEventListener('pointerleave', (e) => {
            left_resize_bar_pushed = false;
        });
        dom.id("right_resize_bar").addEventListener('pointerleave', (e) => {
            right_resize_bar_pushed = false;
        });
        dom.id("bottom_resize_bar").addEventListener('pointerleave', (e) => {
            bottom_resize_bar_pushed = false;
        });
        dom.id("left_resize_bar").addEventListener('pointermove', (e) => {
            if (left_resize_bar_pushed) {
                CONST.LEFT_SIDE_BAR_WIDTH += e.movementX;
                dom.setVar("--left-side-bar-width", CONST.LEFT_SIDE_BAR_WIDTH + "px");
            }
        });
        dom.id("right_resize_bar").addEventListener('pointermove', (e) => {
            if (right_resize_bar_pushed) {
                CONST.RIGHT_SIDE_BAR_WIDTH -= e.movementX;
                dom.setVar("--right-side-bar-width", CONST.RIGHT_SIDE_BAR_WIDTH + "px");
            }
        });
        dom.id("bottom_resize_bar").addEventListener('pointermove', (e) => {
            if (bottom_resize_bar_pushed) {
                CONST.OUTPUT_VIEW_HEIGHT -= e.movementY;
                dom.setVar("--output-view-height", CONST.OUTPUT_VIEW_HEIGHT + "px");
            }
        });
    }
};
