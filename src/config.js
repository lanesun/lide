/*
~
~ 这是Lcore.js的配置文件，简单说来就是Lcore加载完成后执行的脚本（Lcorerc.js）
~ 你可以在这里使用require函数来加载你要使用的模块
~ 你也可以用require函数来预加载之后要使用的模块
~ require函数返回的是一个Promise对象，结果为对应的模块对象，所以你可以使用async函数来进行异步调用，这也是我推荐的方式
~ 关于Prromise和Async函数可以参考MDN的对应页面
~
*/
;(async function () {
    let require = Lcore.require;
    let requireCSS = Lcore.requireCSS;
    const dom = await require('lib/dommer');
    dom.add(document.body, (await require('html/main-html')).data);
    await requireCSS('style/main');
	await Lcore.require('script/main');
})();
