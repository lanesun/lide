;moudle = function (CONST, core, require) {

    CONST.MENU_BAR_HEIGHT = 32;
    CONST.TOOLS_BAR_HEIGHT = 48;
    CONST.LEFT_SIDE_BAR_WIDTH = 320;
    CONST.RIGHT_SIDE_BAR_WIDTH = 320;
    CONST.OUTPUT_VIEW_HEIGHT = 160;
    CONST.STATUS_BAR_HEIGHT = 32;
    CONST.GLOBAL_EVENTS = ['keydown','keyup','keypress'];
};
