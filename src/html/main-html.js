;moudle = async function(self, core, require) {
    self.data = `
<div class="container_elem" id="menu_bar_con">
    <div id="menu_bar">
        <material-button class="menu-button">File</material-button>
        <material-button class="menu-button">Edit</material-button>
        <material-button class="menu-button">View</material-button>
        <material-button class="menu-button">Navigate</material-button>
        <material-button class="menu-button">Code</material-button>
        <material-button class="menu-button">Refactor</material-button>
        <material-button class="menu-button">Run</material-button>
        <material-button class="menu-button">Tools</material-button>
        <material-button class="menu-button">VCS</material-button>
        <material-button class="menu-button">Window</material-button>
        <material-button class="menu-button">Help</material-button>
    </div>
</div>
<div class="container_elem" id="tools_bar_con">
    <div id="tools_bar">
        <material-button class="tool-button" tool-name="Open Folder"><svg viewBox="0 0 24 24"><path d="M6.1,10L4,18V8H21A2,2 0 0,0 19,6H12L10,4H4A2,2 0 0,0 2,6V18A2,2 0 0,0 4,20H19C19.9,20 20.7,19.4 20.9,18.5L23.2,10H6.1M19,18H6L7.6,12H20.6L19,18Z" /></svg></material-button>
        <material-button class="tool-button" tool-name="Open File"><svg viewBox="0 0 24 24"><path d="M14,2H6A2,2 0 0,0 4,4V20A2,2 0 0,0 6,22H18A2,2 0 0,0 20,20V8L14,2M18,20H6V4H13V9H18V20Z" /></svg></material-button>
        <material-button class="tool-button" tool-name="Save"><svg viewBox="0 0 24 24"><path d="M17 3H5C3.89 3 3 3.9 3 5V19C3 20.1 3.89 21 5 21H19C20.1 21 21 20.1 21 19V7L17 3M19 19H5V5H16.17L19 7.83V19M12 12C10.34 12 9 13.34 9 15S10.34 18 12 18 15 16.66 15 15 13.66 12 12 12M6 6H15V10H6V6Z" /></svg></material-button>
        <material-button class="tool-button" tool-name="Save All"><svg viewBox="0 0 24 24"><path d="M1 7H3V21H17V23H3C1.9 23 1 22.11 1 21V7M19 1H7C5.89 1 5 1.9 5 3V17C5 18.1 5.89 19 7 19H21C22.1 19 23 18.1 23 17V5L19 1M21 17H7V3H18.17L21 5.83V17M14 10C12.34 10 11 11.34 11 13S12.34 16 14 16 17 14.66 17 13 15.66 10 14 10M8 4H17V8H8V4Z" /></svg></material-button>
    </div>
</div>
<div class="container_elem" id="left_side_bar_con"></div>
<div class="container_elem" id="main_view_con"></div>
<div class="container_elem" id="output_view_con"></div>
<div class="container_elem" id="right_side_bar_con"></div>
<div class="container_elem" id="status_bar_con"></div>
<div class="resize_bar" id="left_resize_bar"></div>
<div class="resize_bar" id="right_resize_bar"></div>
<div class="resize_bar" id="bottom_resize_bar"></div>
`;
}
;
