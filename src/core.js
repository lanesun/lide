/*
~
~          Keep Simple.
~   A Moudle Core by Lane Sun :)
~
*/
window.Lcore = {};

(function (core) {
    try {
        core.Moudles = new Map([["core", core]]);
        let promises = new Map();
        core.require = async (name) => {
            if (core.Moudles.has(name)) {
                return core.Moudles.get(name);
            } else if (promises.has(name)) {
                return await promises.get(name);
            } else {
                return await core.init(name);
            }
        };
        core.init = (name) => {
            let promise = new Promise(resolve => {
                let elem = document.createElement('script');
                elem.type = "text/javascript";
                elem.onload = async () => {
                    let mod = {};
                    let moudle = window.moudle;
                    window.moudle = null;
                    await moudle(mod, core, core.require);
                    core.Moudles.set(name, mod);
                    promises.delete(name);
                    resolve(mod);
                };
                elem.src = name + ".js";
                document.body.appendChild(elem);
            });
            promises.set(name, promise);
            return promise;
        };
        core.CSSs = new Set();
        let css_promises = new Set();
        core.requireCSS = async (name) => {
            if (core.CSSs.has(name)) {
                return;
            } else if (css_promises.has(name)) {
                return await css_promises.get(name);
            } else {
                return await core.initCSS(name);
            }
        };
        core.initCSS = (name) => {
            let promise = new Promise(resolve => {
                let elem = document.createElement('link');
                elem.rel = "stylesheet";
                elem.onload = async () => {
                    core.CSSs.add(name);
                    css_promises.delete(name);
                    resolve();
                };
                elem.href = name + ".css";
                document.head.appendChild(elem);
            });
            css_promises.add(name, promise);
            return promise;
        };
        window.moudle = null;
    } catch (e) {
        console.error("[CORE]core init error\n" + e.toString());
    }
})(window.Lcore);
